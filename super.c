#include <stdio.h>

#define SUPER_INTEGER	1
#define SUPER_TREE	1
#define SUPER_GRAPH	1
#define SUPER_MATRIX	1
#define SUPER_STRING	1

typedef struct integer_node {
  int num;
  struct integer_node *next_node;
  struct integer_node *right_child;
  struct integer_node *left_child;
}int_node;

int is_in_integer_node_list(int_node *list) {
  // returns true if integer is in integer_node list.
  
}

int index_in_integer_node_list(int_node *list) {
  // returns index of integer in integer_node list.
  
}

void clear_integer_node_list(int_node *list) {
  // clears integer node list.

}

int_node * integer_list_from_integer(int num) {
  // returns an integer list converted, made from an integer.
  
}

int integer_from_integer_list(int_node *list) {
  // returns an integer converted, made from an integer list.
  
}

void delete_integer_from_list(int_node *list, int n) {
  // deletes n instances of an integer from the int_node list, deletes all instances if n is zero.
  
}

void length_of_integer_list(int_node *list) {
  // returns length of the integer list.
  
}

void insert_integer_at_index(int_node *list, int n) {
  // inserts integer at nth index, or at last by default.
  
}

void show_integer_list(int_node *list) {
  // displays integer list on screen.
  
}

void push_integer(int_node *list, int num) {
  // pushes an integer onto an integer list.
  
}

int pop_integer(int_node *list) {
  // pops a node from an integer list, and returns the integer found.
  
}

int_node * reverse_integer_list(int_node *list) {
  // reverses an integer list, and returns a pointer to its head.
  
}

void concat_integer_list(int_node *a, int_node *b) {
  // concats integer list 'b' at the end of integer list 'a'.
  
}

int_node * add_integer_lists(int node *a, int *b) {
  // returns a pointer to the additive result of two int_node lists.
  
}

int_node * subtract_integer_lists(int node *a, int *b) {
  // returns a pointer to the subtracted result of two int_node lists.
  
}

int_node * multiply_integer_lists(int node *a, int *b) {
  // returns a pointer to the multiplicative result of two int_node lists.
  
}

int_node * divide_integer_lists(int node *a, int *b) {
  // returns a pointer to the divided result of two int_node lists.
  
}

int is_greater_integer_list(int_node *list) {
  // returns true if first list represents a higher integer, otherwise false.
  
}

int is_smaller_integer_list(int_node *list) {
  // returns true if first list represents a lower integer, otherwise false.
  
}

void swap_two_integers(int *a, int *b) {
  // swaps two integers using xor.
  
}

int min_of_two_integers(int a, int b) {
  // returns minimum of two integers.
  if(a < b)
    return a;
  else
    return b;
}

int min_of_list_of_integers(int_node *list) {
  // returns minimum of a list of integers.

}

int max_of_list_of_integers(int_node *list) {
  // returns maximum of a list of integers.

}

int max_of_two_integers(int a, int b) {
  // returns maximum of two integers.
  if(a > b)
    return a;
  else
    return b;
}

int sum_of_digits_of_integer(int num) {
  // returns sum of digits of an integer.

}

int reverse_of_integer(int num) {
  // returns reverse of a integer.
  
}

char * reverse_of_string(char *str) {
  // returns reverse of a string.
  
}

int is_palindrome_integer(int num) {
  // returns true if integer is palindrome.

}

int is_palindrome_string(char *str) {
  // returns true if string is palindrome.

}

int nth_fibonacci_integer() {
  // returns nth fibonacci integer.
  // fibonnaci number in log n time
  
}

int is_fibonacci_integer(int num) {
  // returns true if integer exists in fibonacci series.
  
}

int is_prime_integer(int num) {
  // returns true if integer is prime.
  
}

int nth_prime_integer() {
  // returns nth prime integer.
  
}

int_node ** generate_permutations_of_integer_list(int num) {
  // returns a pointer to a list of permutations of lists of integer nodes.
  
}

char ** generate_permutations_of_string(char *str) {
  // returns a pointer to a list of permutations of lists of  string.
  
}

int main() {
  puts("xx");

  // delete node at index function.
  
  // union etc functions operating on integer list.
  
  // mathematical functions.
  
  // functions for tree integer manipulation and stuff.
  
  // matrix functions.
}
